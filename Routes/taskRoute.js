const express = require("express");
// The "taskController" allows us to use the function defined inside it
const taskController = require ("../Controllers/taskController");

// Allows access to HTTP MEthod middlewares that makes it easier to create routes for our application
const router = express.Router();

// Route to get all tasks
router.get("/", (req, res) => {

    taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

// Route to create a Task
// localhost:3001/task it is the same with localhost:3001/task/
router.post("/", (req, res) =>{
	// The "createTask" function needs data from the request body, so we need it to supply in the taskController.createTask(argument).
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Route to delete a task
// Colon (:) is an identifier that helps create a dynamic route which allows in to supply information in url.
// ":id" is a wildcard were you can putthe objectID as the value.
// Ex: localhost:3000/tasks/:id or localhost:3000/tasks/123456
router.delete("/:id", (req, res) =>{
	// If information will be coming from the URL, the data can be accessed from the request "params" property.
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

//Route to update a task
router.patch("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// 1. Create a route for getting a specific task.
router.get("/:id", (req, res) =>{
	taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// 5. Create a route for changing the status of a task to "complete".
router.patch("/:id", (req, res) =>{
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})


// Use "module.exports" to export the router object to be use in the server
module.exports = router;