// It will allow us to use the content of task.js file in the mmodel folder

const Task = require("../Model/task");

// Get all tasks
module.exports.getAllTasks = () =>{
	// The ".then"  method is used to wait for the mongoose "find" method to finish before sending the result back to the route and eventually to the client or postman.
	return Task.find({}).then(result => result);
}

// Create a task
module.exports.createTask = (reqBody) =>{
	// Create a task object based on the mongoose model "Task"
	let newTask = new Task({
		// sets the name property with the value recieved from the postman
		name: reqBody.name
	})
		//Using call back function: newUser.save((saveErr, saveTask) =>{})
		//Using.then method: newTask.save().then((task, error) =>{})
	return newTask.save().then((task, error) =>{
		if(error){
			console.log(error)
			return false
		}
		else{
			// returns the new task object saved in the database to the postman
			return task
		}
	})
}

// Delete a task
// Business Logic
/*
	1. Look for the task with the corresponding id provided in the URL/route
	2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

module.exports.deleteTask = (taskId) =>{
	return Task.findByIdAndRemove(taskId).then((removedTask, err) =>{
		if(err){
			console.log(err);
			return false;
		}
		else{
			return removedTask
		}
	})
}

// Update a Task

// Business Logic
/*
	1. Get the task with the id using the Mongoose method "findById"
	2. Replace the task's name returned from the database with the "name" property from the request body
	3. Save the task
*/

module.exports.updateTask = (taskID, reqBody) =>{
	// findById is the same as "find({"_id":"value"})"
	return Task.findById(taskID).then((result, error) =>{
		if(error){
			console.log(error);
			return false;

		}
		else{
			// we reassign the result name with the request body content.
			result.name = reqBody.name;

			return result.save().then((updatedTaskName, updateErr) =>{
				if(updateErr){
					console.log(updateErr);
					return false;
				}
				else{
					return updatedTaskName
				}
			})
		}
	})
}

/*
Instructions s31 Activity:
1. Create a route for getting a specific task.
2. Create a controller function for retrieving a specific task.
3. Return the result back to the client/Postman.
4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
5. Create a route for changing the status of a task to "complete".
6. Create a controller function for changing the status of a task to "complete".
7. Return the result back to the client/Postman.
8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
9. Create a git repository named S31.
10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
11. Add the link in Boodle.
*/

// 2. Create a controller function for retrieving a specific task.

module.exports.getTask = (taskID) =>{
	return Task.findById(taskID)
}


// 3. Return the result back to the client/Postman. 

//-see postman working.

// 4. Process a GET request at the "/tasks/:id" route using postman to 
get a specific task.
//- see postman working.


// 6. Create a controller function for changing the status of a task to "complete".

module.exports.updateTask = (taskID, reqBody) =>{
	
	return Task.findById(taskID).then((result, error) =>{
		if(error){
			console.log(error);
			return false;

		}
		else{
			
			result.status.default = reqBody.status.default;

			return result.save().then((updatedTaskName, updateErr) =>{
				if(updateErr){
					console.log(updateErr);
					return false;
				}
				else{
					return updatedTaskName
				}
			})
		}
	})
}